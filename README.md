Composer for XenForo
====================

By [Simon Hampel](http://hampelgroup.com/).

This add-on provides [composer](https://getcomposer.org/) integration for XenForo.

This allows you to develop XenForo add-ons using composer autoloading and manage package installation and dependency
management automatically through the use of composer.json files and [packagist](https://packagist.org/).

There is also a "Workbench" tool for add-on developers to autoload their packages directly without requiring the use of 
packagist while developing code.

Installation
------------

**NOTE THIS ADD-ON REQUIRES MODIFICATIONS TO THE XENFORO CORE CODE**

... although the modifications have been made as minor as possible.

You will need Composer installed before you can use this add-on, and you really should be familiar with using Composer
and dependency requirements and source control systems before you begin - that is beyond the scope of this document.
 
If you haven't already installed Composer, follow the [Getting Started](https://getcomposer.org/doc/00-intro.md) guide.

Once you have verified that composer is installed, copy the `Autoloader.php` and `Workbench.php` files from this 
repository into a directory called Composer under your XenForo library directory.

For example, `library/Composer/Autoloader.php` and `library/Composer/Workbench.php`

Create a basic `composer.json` file in the root of your XenForo installation:

    :::json
    {
    	"require": {
    		"symfony/finder": "~2.6"
    	}
    }

The `symfony/finder` package is required by the Workbench autoloader.

You can create a more complete composer.json file if you wish - but it isn't required, since we aren't submitting our
XenForo installation to Packagist or anything\!

Run the `composer update` command to create the vendor directory and autoload files and load any dependencies.

    :::bash
    $ composer update

Now you will need to make two small edits to the `index.php` and `admin.php` core XenForo files to link our autoloader 
in.

Edit the `index.php` in the root of your XenForo installation.

Look for the following code:

    :::php
    require($fileDir . '/library/XenForo/Autoloader.php');
    XenForo_Autoloader::getInstance()->setupAutoloader($fileDir . '/library');

Insert the following two lines *between* these above lines:
    
    :::php
    require($fileDir . '/library/Composer/Autoloader.php');
    XenForo_Autoloader::setInstance(new Composer_Autoloader($fileDir . '/vendor'));

This is what the code block should now look like once edited:
    
    :::php
    require($fileDir . '/library/XenForo/Autoloader.php');
    
    require($fileDir . '/library/Composer/Autoloader.php');
    XenForo_Autoloader::setInstance(new Composer_Autoloader($fileDir . '/vendor'));
    
    XenForo_Autoloader::getInstance()->setupAutoloader($fileDir . '/library');    

This code creates a new `Composer_Autoloader` instance (which extends the base `XenForo_Autoloader` class), and sets
this new autoloader as the instance to use for all autoloading tasks in the future.

You will also need to make exactly the same change to `admin.php` in the root of your XenForo installation.

**DON'T FORGET TO UPDATE THESE FILES WHENEVER YOU UPGRADE YOUR XENFORO INSTALLATION**

###Workbench Installation###

To set up the Workbench on your development server, create an empty `workbench.json` file in your XenForo library
directory, for example:

    :::bash
    $ touch library/workbench.json
    
The contents of the workbench.json file is currently ignored, but may be used for workbench-specific configuration in
the future, at which point it will need to become a valid JSON file.

The Workbench autoloader checks for the presence of this file, and if it finds it, the autoloader will search all 
subdirectories of the XenForo library folder for autoload.php files created by Composer and require them automatically.

**DO NOT create this workbench.json file on your production server**, since it will significantly slow down the 
performance of your XenForo website.

Usage
-----

If you have XenForo add-ons which have been developed using composer autoloading and are hosted on 
[Packagist](https://packagist.org/), you can load them automatically by defining them in your XenForo root 
composer.json file and then running `composer update` to load the add-on and any dependencies.

    :::json
    {
    	"require": {
    		"symfony/finder": "~2.6",
    		"example/add-on": "~1.0"
    	}
    }

At the moment, you'll need to manually update the add-on xml file within the XenForo admin interface. We hope to be able
to automate this process in a future release.

Note that this version does not currently handle add-ons with media assets such as images or JavaScript files. We hope
to support this in a future release.

Package Development
-------------------

To develop a package using the XenForo Composer Workbench, first create the `workbench.json` file in the XenForo library
directory as described in the installation section.

Next, create a subdirectory under the library directory for your add-on to live in.

    :::bash
    $ cd library
    $ mkdir Example
 
Change to that directory and run `composer init` to initialise a new composer project in that directory, making sure you
complete all of the required fields, including `name` and `description`. It is also recommended that you specify a
`license` for your add-on as well.

    :::bash
    $ cd Example
    $ composer init

We suggest putting the source files in a directory called `src` under your add-on root.
 
    :::bash
    $ cd library/Example
    $ mkdir src

For example, if we were developing a package named Example and we follow the XenForo naming pattern for our classes so
we might define a public controller `Example_ContollerPublic_MyController`, which would be defined in the file
`library/Example/src/Example/ControllerPublic/MyController.php`.

Edit your `composer.json` file and add a psr0 autoloading directive so that composer knows where to find your source 
files.

    :::json
    {
    	"require": {
    		
    	},
    	"autoload": {
        	"psr-0": {
            	"Example_": "src/"
        	}
    	}
    }
    
Note the underscore at the end of `Example_` and the slash at the end of `src/`.

If you have defined dependencies in the 'require' section, run `composer update` to load those dependencies, otherwise
you can just run `composer dump-autoload` to generate the autoload files based on your new psr-0 directive.

Now, our autoloader will know where to look for any classes which start with `Example_`.
