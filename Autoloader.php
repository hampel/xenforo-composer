<?php

/**
* Extends the base XenForo autoloader class.
*/
class Composer_Autoloader extends XenForo_Autoloader
{
	protected $vendorDir;

	public function __construct($vendorDir = null)
	{
		if (is_null($vendorDir))
		{
			throw new Exception('Must supply the full path to the Composer vendor director');
		}

		$this->vendorDir = $vendorDir;

		parent::__construct();
	}

	protected function _setupAutoloader()
	{
		if (!file_exists($this->vendorDir . '/autoload.php'))
		{
			throw new Exception('Please run "composer dump-autoload" in your XenForo root directory to generate vendor autoload files');
		}

		// load the root Composer autoload file
		require_once $this->vendorDir . '/autoload.php';

		// if we're on our dev server, we can optionally tell the system to search for addons we're developing and load
		// any Composer autoload files there - since they may not be registered in the root composer.json yet
		//
		// right now, the system just looks for a placeholder file "library/workbench.json", which can be blank - it is
		// not currently parsed, although we may add some additional configuration to it in later versions
		//
		// if using this workbench feature, remember to run 'composer update' for each of the addons which use composer
		// so that the vendor autoload files are generated
		//
		if (file_exists($this->getRootDir() . '/workbench.json'))
		{
			require_once $this->getRootDir() . '/Composer/Workbench.php';
			Composer_Workbench::start($this->getRootDir());
		}

		parent::_setupAutoloader();
	}

	public function autoload($class)
	{
		// try the XenForo autoloader first
		if (parent::autoload($class))
		{
			return true;
		}

		// okay, if all else fails, let's try the built-in autoloaders, which should pick up our composer stuff
		if (class_exists($class) || interface_exists($class))
		{
			return true;
		}

		return false;
	}
}